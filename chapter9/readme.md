# Kubernetes

## Установка миникуба

```bash
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb
```

Настраиваем драйвер докера
```bash
minikube config set driver docker
```

```bash
minikube start/stop/restart
```

Контекст - сущность, объединяющая кластер пользователей.
![img_1.png](img_1.png)

Для управления устанавливаем kubectl (команды)
```bash
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl

chmod +x kubectl 
sudo cp kubectl /usr/local/bin/
kubectl version
```

Посмотреть текущий контекст
```bash
kubectl config current-context
```

Посмотреть его конфигурацию
```bash
kubectl config get-contexts minikube
```

Запуск, backend может быть папкой где лежат конфиги
```bash
kubectl apply -f backend
```

Посмотреть логи пода
```bash
kubectl logs <имя Pod'а > --previous 
```

Редактирование конфига налету (нежелательно)
```bash
kubectl edit pod backend
```

Посмотреть какие виды ресурсов есть:
```bash
kubectl api-resources
```

Зайти в под/контейнер (nginx - имя пода)
```bash
kubectl exec -it nginx -- bash
```

Прокидывание портов на машину, где находится kubectl
```bash
kubectl port-forward nginx 8080:80
```

Информация о верхнеуровневых сущностях, поды сервисы и пр
```bash
kubectl explain <сущность>
```

Информация о самом объекте
```bash
kubectl describe pod nginx
```

Посмотреть все сущности
```bash
kubectl get all
```


Синтаксис команд:

kubectl [command] [TYPE] [NAME] [flags]

    [command] — при работе с ресурсами кластера команда определяет глагол выполняемого действия, например, get, create или delete.
    [TYPE] — название типа ресурса кластера. Содержимое кластера мы разберём позже.
    [NAME] — имя ресурса в кластере.
    [flags] — дополнительные флаги, например, для изменения формата вывода.


## Структура k8s

![img.png](img.png)

Control Plane состоит из нескольких компонентов-сервисов:
* kube-api-server
* etcd
* kube-scheduler
* kube-controller-manager


Допустим, на понятном для Kubernetes языке мы попросили cоздать Pod и сохранили в YAML формате в pod.yaml. Чуть позже поговорим об этом подробнее, а пока надо лишь поверить, что так можно делать и это работает:

Порядок взаимодействия

* Применяем в кластер команду $ kubectl apply -f pod.yaml.
* kubectl смотрит в конфиг ~/.kube/config, ищет нужный kube-api-server сервер и отправляет ему запрос.
* kube-api-server получает запрос на создание Pod'а, затем проверяет и если всё ок, сохраняет его в etcd. Кроме того, через kubectl он «выводит» пользователю сообщение о том, что Pod создан, или ошибку, если что-то пошло не так.
* etcd сообщает kube-api-server, что в нём изменилась информация, а kube-api-server вызывает kube-scheduler.
* kube-scheduler выбирает для Pod'а узел, на котором он должен быть запущен. Далее kube-scheduler отдаёт эту информацию kube-api-server, потом kube-api-server обновляет информацию о Pod'е в etcd.
* etcd снова сообщает kube-api-server, у него что-то изменилось. Потом kube-api-server идёт к kubelet на «выбранном» узле и говорит, что надо бы Pod запустить.
* kubelet идёт к Container Runtime и говорит, надо бы создать контейнер(ы). Тогда Container Runtime выполняет просьбу и отдаёт информацию обратно kubelet, которая отдает её kube-api-server и, наконец, kube-api-server записывает её в etcd. 

kube-controller-manager так же отслеживает изменения в кластере через kube-api-server, как в примере выше, и вносит необходимые изменения в кластере. Например, количество реплик у Pod'a должно быть 3, а запушено всего 2. Тогда kube-controller-manager создаст недостающий Pod

kube-proxy, отвечает за сеть. Он, как и все, отслеживает состояние кластера через kube-api-server и поддерживает актуальное состояние сети с помощью правил iptables, которые отправляют трафик в «правильные» Pod'ы и балансировка!

Дополнительно также ставят DNS (CoreDNS) и kubernetes dashboard

## Сети в кубере

три уровня сети в кластере Kubernetes:

* **HostNetwork** — это обычная сеть рабочей ноды. С её помощью узлы кластера общаются с Control Plane и по адресу в этой сети можно подключиться к рабочей ноде по ssh. Приложение можно сделать доступным на каком-либо порту одной или всех рабочих нод.
* Кроме обычной сети, на каждой рабочей ноде создаётся внутрення сеть для подов — **PodNetwork**. Каждый Pod на ноде получает в этой сети IP-адрес и способен видеть соседние поды. При этом доступ к контейнеру внутри ноды будет осуществляться через IP-адрес поды + порт контейнера. Можно настраивать доступ к приложению, используя адрес внутренней сети.
* **ServiceNetwork**. Service — это сущность Kubernetes, которая позволяет на более высоком (логическом) уровне настраивать доступы к приложениям. С помощью сервисов можно, например, открыть порт на всех нодах, по которому будет доступно приложение, или использовать балансировку

## Компоненты

![img_2.png](img_2.png)

Документация по куберу яндекса
https://yandex.cloud/ru/docs/managed-kubernetes/

ПОказывает какие сущности есть в кубере, а также их shortnames
```bash
kubectl api-resources
```

## Паттерны для контейнеров в поде:

**sidecar container** — если дополнительный контейнер должен быть запущен рядом с основным, sidecar расширяет функциональность основного контейнера, не изменяя его. Все контейнеры в Pod'е работают параллельно. Можно запускать несколько sidecar'ов, а сам sidecar чаще всего запускает 
prometheus-exporter для сборки метрик с приложения в Pod'е или сборщик логов (про метрики и логи будет позже).

**init container** — если нужно выполнить какие-то действия до запуска основного контейнера. Init-container запускается, что-то делает и завершается, и только после этого запускается основной контейнер. Можно запустить сколько угодно init-container'ов, но основной запустится только после того, как отработают все init'ы. В init выносятся различные таски для инициализации приложения или проверки доступности внешних зависимостей.

**adapter container** — нужен, чтобы адаптировать существующие приложения к требуемому интерфейсу. Как и sidecar, расширяет функциональность основного контейнера. Проще говоря, есть наше приложение и оно предоставляет какой-то интерфейс для внешних пользователей. Внезапно некоторым пользователям он не нравится, и они хотят другой интерфейс. Чтобы не переписывать приложение, можно соорудить легковесный сервис и запаковать его в adapter container.

**ambassador container** — специальный тип sidecar контейнеров, куда запаковывают «кусочки», которые упрощают доступ к сервисам за пределами Pod'а и предоставляют единый интерфейс для взаимодействия с этими внешними сервисами.


## Манифест запуска сущности в кубере

```yaml
---
apiVersion: v1
kind: Pod
metadata:
  name: nginx
spec:
  containers:
   - name: nginx
     image: nginx:1.21.6
     imagePullPolicy: IfNotPresent
     resources:
       requests:
         memory: "64Mi"
         cpu: "100m"
       limits:
         memory: "128Mi"
         cpu: "200m" 
```

Посмотреть доку по верхним уровням манифеста:
```bash 
kubectl explain pod
```

## Service

Сущность для взаимодействия с подами

```yml
---
apiVersion: v1
kind: Service
metadata:
   name: nginx
   labels:
     app: nginx
spec:
  type: ClusterIP
  ports:
    - port: 80
      protocol: TCP
      targetPort: 80
  selector:
    app: nginx 
```

## Persisted Volume

Создаем диск в облаке, потом подключаем его в кластер

```yml
---
kind: PersistentVolume
apiVersion: v1
metadata:
  name: nginx-pv
spec:
  capacity:
    storage: 4Gi
  storageClassName: "yc-network-hdd"
  accessModes:
    - ReadWriteOnce
  csi:
    driver: disk-csi-driver.mks.ycloud.io
    fsType: ext4
    volumeHandle: epdt13t41isrit225vdv 
```
### Persistent Volume Claim

Запрос на выделение ресурса хранилища для приложения

```yml
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: nginx-pvc
  labels:
    app: nginx
spec:
  storageClassName: "yc-network-hdd"
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 4Gi
  volumeName: nginx-pv 
```

И добавляем обращение к ресурсу в под:

```yml
---
apiVersion: v1
kind: Pod
metadata:
  name: nginx
  labels:
    app: nginx
spec:
  containers:
   - name: nginx
     image: nginx:1.21.6
     volumeMounts:
       - name: nginx-pvc-volume
         mountPath: /usr/share/nginx/html/
  volumes:
    - name: nginx-pvc-volume
      persistentVolumeClaim:
        claimName: nginx-pvc
```

## ConfigMap

Данные записаны в виде ключ-значение
```yml
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: nginx-conf
data:
  nginx.conf: |
    user  nginx;
    worker_processes  auto;
    error_log  /var/log/nginx/error.log notice;
    pid        /var/run/nginx.pid;
    events {
        worker_connections  1024;
    }

    http {
        include       /etc/nginx/mime.types;
        default_type  application/octet-stream;

        log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                          '$status $body_bytes_sent "$http_referer" '
                          '"$http_user_agent" "$http_x_forwarded_for"';

        access_log  /var/log/nginx/access.log  main;

        sendfile        on;
        keepalive_timeout  65;
        server {

          listen       80;
          listen  [::]:80;
          server_name  localhost;

          location / {
              root   /usr/share/nginx/html;
              index  index.html index.htm;
          }

          error_page   500 502 503 504  /50x.html;
          location = /50x.html {
              root   /usr/share/nginx/html;
          }
      }

    } 
```

а теперь используем:

```yml
...
 volumeMounts:
   - name: nginx-conf
     mountPath: /etc/nginx/nginx.conf
     subPath: nginx.conf
     readOnly: true
...
 volumes:
   - name: nginx-conf
     configMap:
       name: nginx-conf
       items:
         - key: nginx.conf
           path: nginx.conf 
```


## Secrets

```yml
---
apiVersion: v1
kind: Secret
metadata:
  name: nginx
  labels:
    app: nginx
data:
  host: ZGItcG9zdGdyZQ==
  dbname: cG9zdGdyZXM=
  username: cG9zdGdyZXM=
  password: UVdFUlRZMTIz
type: Opaque 
```

Обращение к секрету:

```yml
spec:
  containers:
   - name: nginx
     image: nginx:1.21.6
     env:
     - name: DB_HOST
       valueFrom:
         secretKeyRef:
           name: nginx
           key: host 
```

## Чекеры состояния пода 

В двух словах о том, что проверяют пробы:

* livenessProbe — запущен ли контейнер?
* startupProbe — запущено ли приложение внутри контейнера?
* readinessProbe — приложение готово работать с пользователями и можно ли пускать трафик?
* 

livenessProbe - проверка живучести приложения в контейнере

* **HTTP GET** — если код ответа 2xx или 3xx, то проверка выполнена успешно; во всех иных случах — нет.
* Проверка **TCP-сокета** — пытается открыть подключение к сокету: если вышло — проверка выполнена успешно, а иначе — увы.
* Проверка **Exec** (выполняет произвольную команду внутри контейнера) — если код выхода = 0, то проверка выполнена успешно.





# Helm

Команды доки
https://helm.sh/ru/docs/intro/using_helm/

Немного полезностей, которые помогут при работе:

    Ключи --wait и --timeout 600 могут быть очень полезны при работе, они в паре говорят Helm подождать ответа от Kubernetes 10 минут (600 секунд) вместо 5 минут по умолчанию. Время ожидания может зависеть от специфики кластера или пайплайна развёртки вашего приложения, можно изменять.
    Взять текущие переменные, которые использовались в чарте -> поменять значения переменных (если нужно) -> обновить релиз с новыми значениями из файла:

```bash
helm get values nginx > my.values
helm upgrade nginx./nginx-test --values my.values
```

    Ключ --reuse-values говорит Helm использовать при обновлении значения переменных из последнего релиза.


Если дописать ключ --keep-history, то история релизов останется и можно будет применить rollback, иначе удалится вся информация о релизе.
```bash
helm uninstall nginx --keep-history
```



helm repo add heml-test-repository https://nexus.praktikum-services.tech/repository/std-034-60-test-helm/ --username std-034-60 --password lyJ9k34D 

helm install heml-test-repository nginx --version 0.0.1

