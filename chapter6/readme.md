# Итого инструменты

gitlab + ci - управление разработкой, потоком разработки, сборкой тестированием и деплоем приложения (из хранилища)
nexus - хранилище сборок
terraform - управление созданием виртуальных машин
ansible - настройка виртуальных машин, деплой на множество серверов (из хранилища)
docker - механизм контейнеризации, используется в течении потока разработки (сборка, тестирование)
sonarcube - статические тесты приложения


# Postgres

пароль
Testusr1234

```bash
psql postgresql://$PSQL_USER:$PSQL_PASSWORD@$PSQL_HOST:$PSQL_PORT/$PSQL_DBNAME

psql postgresql://std-034-60:Testusr1234@rc1d-vx5o4ou4u650elu5.mdb.yandexcloud.net:6432/std-034-60

psql "host=rc1d-vx5o4ou4u650elu5.mdb.yandexcloud.net port=6432 sslmode=verify-full dbname=std-034-60 user=std-034-60 target_session_attrs=read-write"

```

Вариант подключения в unit systemd

```
Environment=SPRING_DATASOURCE_URL=jdbc:postgresql://rc1b-zpl4km6ny2imb2xf.mdb.yandexcloud.net:6432/std-016-000
Environment=SPRING_DATASOURCE_USERNAME=std-016-000
Environment=SPRING_DATASOURCE_PASSWORD=Testusr1234

ExecStart=java -jar /var/jarservice/sausage-store.jar
```

либо так

```
ExecStart=java -jar sausage-store.jar --spring.datasource.url=jdbc:postgresql://${PSQL_HOST}:${PSQL_PORT}/${PSQL_DBNAME}?ssl=true&sslmode=verify-full&sslrootcert=/home/jarservice/root.crt --spring.datasource.username=${PSQL_USER} --spring.datasource.password=${PSQL_PASSWORD}
```

Работа с дампами postgres
```bash
# создание дампа в 8 потоков, в формате директории
pg_dump postgresql://$PSQL_USER:$PSQL_PASSWORD@$PSQL_HOST:$PSQL_PORT/$PSQL_DBNAME -j 8 -Fd -f /tmp/dump.dir

# выгрузка дампа в формате директории
pg_restore -d "postgresql://$PSQL_USER:$PSQL_PASSWORD@$PSQL_HOST:$PSQL_PORT/$PSQL_DBNAME" --format=d /tmp/dump.dir/

```


Вариация юнита сервиса systemd
```bash
cat /etc/systemd/system/sausage-store-backend.service
```

```unit file (systemd)

[Unit]
Description=Sausage-store backend service
After=multi-user.target

[Service]
User=jarservice
EnvironmentFile=/home/jarservice/vars.env
WorkingDirectory=/home/jarservice/
ExecStart=java -jar sausage-store.jar --spring.datasource.url=jdbc:postgresql://${PSQL_HOST}:${PSQL_PORT}/${PSQL_DBNAME}?ssl=true&sslmode=verify-full&sslrootcert=/home/jarservice/root.crt --spring.datasource.username=${PSQL_USER} --spring.datasource.password=${PSQL_PASSWORD} --spring.data.mongodb.uri=mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_HOST}:27018/${MONGO_DATABASE}?tls=true --spring.data.jdbc.repositories.enabled=false -Djavax.net.ssl.trustStore=/home/student/mystore -Djavax.net.ssl.trustStorePassword=superpass
Restart=always
StandardOutput=file:/opt/log/sausage-store-backend.log
StandardError=file:/opt/log/sausage-store-backend.log
SuccessExitStatus=143

[Install]
WantedBy=multi-user.target
```


Посмотреть параметры процессора и памяти
```bash
ps -aux --sort -pcpu
```


обновление постгрес
```bash
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget -qO- https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo tee /etc/apt/trusted.gpg.d/pgdg.asc &>/dev/null
sudo apt update
sudo apt install postgresql-client -y
```


Подключение к БД postgres

```bash
# Установка сертификата:
mkdir -p ~/.postgresql && \
wget "https://storage.yandexcloud.net/cloud-certs/CA.pem" \
--output-document ~/.postgresql/root.crt && \
chmod 0600 ~/.postgresql/root.crt

# Установить зависимости:
sudo apt update && sudo apt install --yes postgresql-client

# Подключение к БД:
psql "host=rc1d-vx5o4ou4u650elu5.mdb.yandexcloud.net \
    port=6432 \
    sslmode=verify-full \
    dbname=std-034-60 \
    user=std-034-60 \
    target_session_attrs=read-write"
```

```bash
psql postgresql://std-034-60:Testusr1234@rc1d-vx5o4ou4u650elu5.mdb.yandexcloud.net:6432/std-034-60 
```


# Подключение к БД Mongo

```bash
# Установка сертификата:
mkdir -p ~/.mongodb && \
wget "https://storage.yandexcloud.net/cloud-certs/CA.pem" \
--output-document ~/.mongodb/root.crt && \
chmod 0644 ~/.mongodb/root.crt

# Подключение к Mongo:
mongosh --norc \
        --tls \
        --tlsCAFile /home/<домашняя директория>/.mongodb/root.crt \
        --host 'rs01/rc1d-cc6d837rwcu7jwa4.mdb.yandexcloud.net:27018' \
        --username std-034-60 \
        --password Testusr1234 \
        std-034-60

mongosh --norc --tls --tlsCAFile C:/Users/info/.mongodb/root.crt --host 'rs01/rc1d-cc6d837rwcu7jwa4.mdb.yandexcloud.net:27018' --username std-034-60 --password Testusr1234 std-034-60

# Строка для СУБД
mongodb://std-034-60:Testusr1234@rc1d-cc6d837rwcu7jwa4.mdb.yandexcloud.net:27018/?tls=true&tlsCAFile=C%3A%5CUsers%5Cinfo%5C.mongodb%5Croot.crt&replicaSet=rs01

```

