https://medium.com/codex/terraform-best-practices-how-to-structure-your-terraform-projects-b5b050eab554

# Terraform best practices — how to structure your Terraform projects

When starting out with Terraform it’s hard to know what is considered ‘best practice’ in a number of areas.

This post is the first in the series which focuses on point 1 in the list, ‘how to structure your Terraform projects’.

    Use a consistent file structure across your projects.
    Use modules wherever possible.
    Use a consistent naming convention.
    Use a consistent format and style.
    Hold your state file remotely, not on your local machine.
    Avoid hardcoding variables.
    Fewer resources in a project are easier and faster to work with.
    Limit resources in the project to reduce the blast radius.
    Test your code.

![img.png](img.png)

## Terraform project structure

The way you split your Terraform code into files is subjective. For a very simple project, it may absolutely be easier to lump everything together in one file and forget about it. However, as best practice, I like to structure my files for every project I create large or small in the same way, that way the project can easily be scaled without having to unpick it if you decide to restructure.

Modules will be tackled in a separate post, for now, just know that they are really just folders that hold config files that are coded in such a way that the code can be re-used. If you use modules, you will have folders in your project structure. These can also be nested, although it is recommended not to go more than 3/4 levels deep and to avoid this if possible to reduce complexity.
README.md

Each repository should contain a README.MD file in the root. This is the first port of call for a user to learn about the project. It should describe:

- What the project does
- Why the project is useful
- How users can get started with the project
- Where users can get help with your project
- Who maintains and contributes to the project

main.tf

The next port of call for anyone looking at your code. I like to specify the backend storage method for the state file and the required providers here. Locals and Data blocks could also be included here if needed.

![img_1.png](img_1.png)

**provider.tf**

Some people like to put the providers in a separate providers.tf file which might be a good idea if you have lots of them, or maybe the same providers but with different versions.

**variables.tf**

Defines the variables needed.
![img_2.png](img_2.png)

**output.tf**

Defines all the outputs. The below example outputs the express route gateway ID and the provisioning state:
![img_3.png](img_3.png)

**resources.tf**

For a small project, it is easiest to put all resources under one resources.tf file, this will very quickly become confusing.

It is better to split your files by resource types, e.g. have a file named resource_groups.tf that contains all your resource group definitions, then have another file called networking.tf that contains all your VNET definitions, subnets, and NSGs, another file that might define your VMs called vm.tf…and so on. You may even split up the aforementioned VNETS, subnets and NSGs into separate files!

Better still, instead of defining resources in these files, they should be calling modules where possible, which would actually define the resources.

A simple example of a resource_groups.tf file creating a resource group:

![img_4.png](img_4.png)

An example of a file calling a module that creates a VM (could be called vm.tf):

![img_5.png](img_5.png)

**terraform.tfvars**

Variable values that are secret. I like to split them away from the auto.tfvars files. They are generally variables used for authentication or setting environment variables.

***.auto.tfvars**

Variable values that are read in automatically. For example, I’ve shown a file called vwan.auto.tfvars that defines variables for a VWAN module.

![img_6.png](img_6.png)

**.gitignore**

Anything listed in this file will be ignored when you check into source control. A good .gitignore template to use is listed below (source gitignore/Terraform.gitignore at master · github/gitignore · GitHub)
https://github.com/github/gitignore/blob/master/Terraform.gitignore

```
    # Local .terraform directories
    **/.terraform/*
    
    # .tfstate files
    *.tfstate
    *.tfstate.*
    
    # Crash log files
    crash.log
    
    # Exclude all .tfvars files, which are likely to contain sentitive data, such as
    # password, private keys, and other secrets. These should not be part of version
    # control as they are data points which are potentially sensitive and subject
    # to change depending on the environment.
    #
    *.tfvars
    
    # Ignore override files as they are usually used to override resources locally and so
    # are not checked in
    override.tf
    override.tf.json
    *_override.tf
    *_override.tf.json
    
    # Include override files you do wish to add to version control using negated pattern
    #
    # !example_override.tf
    
    # Include tfplan files to ignore the plan output of command: terraform plan -out=tfplan
    # example: *tfplan*
    
    # Ignore CLI configuration files
    .terraformrc
    terraform.rc
```

Splitting projects into multiple Terraform files makes reading and understanding how the Terraform code works much more palatable than one monolithic file.

Local modules should follow the same breakdown but may be simpler, e.g. just contain the necessary variables.tf, main.tf, and output.tf where required.

Let me know if I’ve missed anything and stay tuned or subscribe!

Want more Terraform content? Check out my other articles on Terraform here!

Cheers! 🍻

Статьи по терраформ
https://jackwesleyroper.medium.com/terraform-related-articles-index-52fab8f11a0b


