# IaC
Ansible, Chef, Puppet
установка ПО на серверах, средства управления конфигурацией

Terraform, Openstack
конфигурирование железа, создание серверов, БД, балансировщиков

Docker, Vagrant, Kubernetes
создание виртуалок, образов, оркестрация?



Получение токена

yc init --federation-id=bpfpfctkh7focc85u9sq

yc config list

Использование grep для windows:
```
yc compute image list --folder-id standard-images|grep ubuntu-20
```
для винды так:
```
yc compute image list --folder-id standard-images|Select-String -Pattern "ubuntu-20"
```


yc iam create-token

export YC_TOKEN=t1.9euelZqazYyejJmempvIj8_IyM7JmO3rnpWamsaRnJfMipWaj86Pis6Niorl8_cqKC5Y-e92Zncy_d3z9yIaLlj573ZmdzL9zef1656Vmp2ezszMms2NjI3Mipuel82a7_zF656Vmp2ezszMms2NjI3Mipuel82aveuelZrOmY2SzM7Px4_LiYuSzJDIlbXrhpzRlp6S0ZCPmpGWm9KMmo2Jmo0.DJ4_nqMEQuu60epSBGlYnONq9IMmYyCs4UewOEDssz2W0GsUAoUBai9i0NtdTAjr54PyjAhmISVY-5Tm_MrwDA

для винды:

set YC_TOKEN=t1.9euelZqazYyejJmempvIj8_IyM7JmO3rnpWamsaRnJfMipWaj86Pis6Niorl8_cqKC5Y-e92Zncy_d3z9yIaLlj573ZmdzL9zef1656Vmp2ezszMms2NjI3Mipuel82a7_zF656Vmp2ezszMms2NjI3Mipuel82aveuelZrOmY2SzM7Px4_LiYuSzJDIlbXrhpzRlp6S0ZCPmpGWm9KMmo2Jmo0.DJ4_nqMEQuu60epSBGlYnONq9IMmYyCs4UewOEDssz2W0GsUAoUBai9i0NtdTAjr54PyjAhmISVY-5Tm_MrwDA


раскладывание конфига терраформ по частям
https://medium.com/codex/terraform-best-practices-how-to-structure-your-terraform-projects-b5b050eab554

выбор платформы в облаке яндекса (resource.platform_id)
https://cloud.yandex.ru/docs/compute/concepts/vm-platforms

ВАЖНО!
Все значения текущие в облаке яндекса надо смотреть через консоль yc, идентификаторы, имена
Например
список подсетей
yc vpc subnets list
список сетей 
yc vpc network list
и так далее


for_each
https://thenewstack.io/how-to-use-terraforms-for_each-with-examples/
http://onreader.mdl.ru/HashicorpInfrastructureAutomationCertification/content/Ch04.html

обучалка
https://www.educative.io/answers/how-to-use-foreach-to-iterate-over-a-list-in-terraform



Запуск конфигурации:
terraform init

проверка (линтер)
terraform validate

terraform plan
terraform apply


🔸Terraform
Запись встречи про Terraform доступна по ссылке
А вот и различные полезности:
1. Пример описания инфраструктуры с помощью модулей и пояснения к нему 
   https://github.com/antonbabenko/terraform-best-practices/tree/master/examples/large-terraform
   https://www.terraform-best-practices.com/examples/terraform/large-size-infrastructure-with-terraform
2. Туториалы по созданию модулей от Hashicorp
   https://developer.hashicorp.com/terraform/tutorials/modules?utm_source=WEBSITE&utm_medium=WEB_IO&utm_offer=ARTICLE_PAGE&utm_content=DOCS
3. Краткая заметка о том, в каком случае стоит разделять описание инфраструктуры с помощью модулей (содержит полезные ссылки)
   https://developer.hashicorp.com/terraform/language/modules/develop

Документация terraform Yandex и по облаку яндекс
https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/vpc_network
https://terraform-provider.yandexcloud.net/Resources/vpc_network
https://cloud.yandex.ru/docs/vpc/operations/network-update
https://cloud.yandex.ru/docs/tutorials/infrastructure-management/terraform-quickstart

статьи по терраформ
https://habr.com/ru/articles/684964/
https://habr.com/ru/companies/nixys/articles/721404/
https://habr.com/ru/articles/683844/

🔸Ansible
Здесь найдете запись вчерашнего вебинара и презентацию от Юры


1. Сборник лучших практик от разработчиков Ansible https://docs.ansible.com/ansible/2.8/user_guide/playbooks_best_practices.html
2. Справочник по синтаксису и семантике Jinja2 с отличными примерами https://jinja.palletsprojects.com/en/3.1.x/templates/
3. Документация по фреймворку тестирования Ansible Molecule + Testinfra фреймворк для тестирования инфраструктуры
   https://ansible.readthedocs.io/projects/molecule/
   https://testinfra.readthedocs.io/en/latest/
4. Ansible Up and Running - отличнейшая книга в новом издании https://www.ansiblebook.com/
5. Ansible Lint - однозначно мастхэвный линтер для проверки конфигурации Ansible (стоит вспомнить про интеграцию с pre-commit) https://github.com/ansible/ansible-lint
6. Худшие практики для Ansible - хороший доклад про то, как все таки не нужно использовать Ansible SCM https://habr.com/ru/articles/536256/


И еще один вебинар на тему: Infrastructure as code


напоминалка, как вывести ключ
https://cloud.yandex.ru/docs/compute/operations/vm-connect/ssh?from=int-console-help-center-or-nav

установка пакетов node Ansible
https://docs.ansible.com/ansible/2.9/modules/npm_module.html

## Ansible

ansible-vault encrypt_string "stu1!den#t" --name student


Пароль для нексуса
ansible-vault encrypt_string "gyr.AP2/" --name nexus_user

ansible-vault encrypt_string "stu3den#t" --name ansible

ansible-play playbook.yml -i inventory/yandexcloud.yml -l backend.host --vault-password-file .vault -C
--check -С запуск тестовый, проверка
--diff -D какие изменения вносятся

ansible-playbook playbook.yaml
