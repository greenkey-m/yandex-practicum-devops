
# Основы работы на линукс

https://code.s3.yandex.net/devops/basic_commands_Linux.pdf

Доступ к менеджерам пакетов
apt, yum, pacman

```useradd``` - добавить пользователя + его домашняя группа

```userdel``` - удалить пользователя

```groupadd``` - добавить новую группу

```passwd``` - изменение пароля пользователей

```cat``` - вывод содержимого (файла), ```tail``` - вывод хвоста файла, ```head``` - вывод начала

```whoami``` - инфа о себе текущая

```uname``` - инфа о системе

```find``` - поиск файлов в директории

```usermod``` - изменение аккаунта пользователя

```ps``` - просмотр процессов

```pidof ***``` - посмотреть идентфикатор процесса process id у запущенного сервиса ***

```groups {user}``` - показать группы пользователя

Важно! При создании пользователя, указывать оболочку по умолчанию - shell 
```
sudo useradd -m -s /bin/bash jarservice
```

## Sudoers

Управление sudoes (права пользователям и группам)
Если указывать команды, то указывать полностью путь.
Если нужно задать для группы, то указывать %groupname.
```
jarservice    ALL=(root) NOPASSWD: /usr/bin/id
```
ALL - указание для каких хостов
root - от какого пользователя запускать. Можно укзаать ALL, ALL:ALL (второй - от какой группы)
/usr/bin/id - команда, можно указать ALL

Настройки для логгирования sudo, а также конфиг для логгирования вывода по sudo
```
Defaults    logfile="/var/log/sudo.log"

Defaults        log_output
Defaults        log_input
Defaults        iolog_dir=/var/log/sudo-io/%{user}
%admin         ALL=(ALL) NOPASSWD: LOG_INPUT: LOG_OUTPUT: ALL
```

## Основы работы

```su jarservice``` - войти как пользователь
```su - jarservice``` - войти с переменными окружения этого пользователя

запуск команды разово 
```su -c 'id'```

для редактирования доступов к sudo /etc/sudoers
специальный редактор
```visudo```

для написания своих правил, в файле есть #includedir /etc/sudoers.d
это значит включение файлов из этой папки

посмотреть переменные окружения
```env```

локальная переменная и глобальная (относительно текущего процесса bash)
```
LOCAL_VAR=A
export GLOBAL_VAR=B
```

прописывать переменные для всех - в файле /etc/profile

шпаргалка по screen
https://byurrer.ru/screen
https://itman.in/ssh-screen/

- ```Ctrl+a c``` — создать ещё одну оболочку (??? включит эмуляцию bash в регионе-окне)
- ```Ctrl+a d``` — выход из сеанса оболочки 
- ```Ctrl-a w``` — список окн-регионов
- ```Ctrl+a "``` — просмотреть список оболочек в текущем сеансе screen (b в текущем окне!!!)
- ```Ctrl+a NUM``` — переключиться на NUM окно, где NUM - это номер окна
- ```Ctrl+a A``` — переименовать окно
- ```Ctrl+a :``` — и написать split разделить окно горизонтально
- ```Ctrl+a |``` — и написать split -v разделить окно вертикально
- ```Ctrl+a tab``` — переключиться в следующий терминал в пределах окна
- ```Ctrl+a Ctrl+a``` — переключиться между предыдущим окном и текущим
- ```Ctrl+a X``` — закрыть текущую оболочку
- ```Ctrl+a Q``` — закрыть всё, кроме текущей оболочки
- ```Ctrl — a :quit``` - завершить текущую сессию!

```screen -ls``` - показать список сессий screen
```screen -r myname``` - войти в сессию
```screen -S myname``` - создание сессии с именем

## Юнит-файл systemd

место для создания собственных юнитов (сервисы): /etc/systemd/system/

```
systemctl list-units --type service --all — просмотр всех юнитов в системе
systemctl start name    — запустить сервис
systemctl stop name    — остановить сервис
systemctl restart name    — перезапустить сервис
systemctl status name — посмотреть статус сервиса
systemctl reload name    — перечитать конфигурацию
systemctl daemon-reload   — перечитать конфигурацию для всех
systemctl try-restart name    — перезапустить, если запущен
systemctl enable name    — включить автозапуск сервиса
systemctl disable name    — отключить автозапуск сервиса
systemctl list-unit-files --type service — список установленных юнит-файлов сервисов
```

Посмотреть процесс-ид сервиса:
```pidof cron```

Просмотр последних записей журнала сервисов
```journalctl -u cron.service -n 20```

За промежуток времени
```sudo journalctl -u monitoring.service --since "2021-07-30 14:10:10" --until "2021-08-02 12:05:50"```

### Раздел Install

- **WantedBy=**: Данная директива, является наиболее распространенным способом определения того, как устройство должно быть включено. Эта директива позволяет вам указать зависимость ( аналогично директиве Wants = в разделе [Unit]). Разница в том, что эта директива включена в вспомогательную единицу, позволяющую первичному блоку оставаться относительно чистым. Когда устройство с этой директивой определено, то будет создана в системе  папка в /etc/systemd/, названная в честь указанного устройства, но с добавлением «.wants» в самом конце. В этом случае будет создана символическая ссылка на текущий блок, создающий зависимость. Например, если текущий блок имеет WantedBy = multi-user.target, каталог с именем multi-user.target.wants будет создан в/etc/systemd/system (если он еще не доступен) и символическая ссылка на текущий блок будут размещены внутри. Отключение этого устройства удаляет связь и удаляет зависимость.
- **RequiredBy=**: Эта директива очень похожа на директиву WantedBy =, но вместо этого указывает требуемую зависимость, которая приведет к сбою активации, если не будет выполнена. Когда включено, юнит с этой директивой создаст каталог, заканчивающийся на .requires.
- **Alias=**: Эта директива позволяет включить устройство под другим именем. Среди других применений это позволяет доступным нескольким провайдерам функции, чтобы соответствующие подразделения могли искать любого провайдера с общим псевдонимом.
- **Also=**: Эта директива позволяет включать или отключать блоки в качестве набора. Здесь можно указать поддерживающие устройства, которые всегда должны быть доступны, когда это устройство активно. Они будут управляться как группа для задач установки.
- **DefaultInstance=**: Для template units (более поздних), которые могут создавать экземпляры блоков с непредсказуемыми именами, это может использоваться как резервное значение для имени, если соответствующее имя не предоставляется.

### Просмотр логов

Просмотр за период по времени
```sudo journalctl -u monitoring.service --since "2021-07-30 14:10:10" --until "2021-08-02 12:05:50"```


# Дисковая подсистема Linux

## Нижние уровни - физические и lvm

```lsblk -l```

    NAME — имя диска
    FSTYPE — тип файловой системы
    LABEL — метка, может содержать человекочитаемое описание диска
    UUID — физический ID устройства
    FSAVAIL — место на диске, доступное для файловой системы (обычно меньше физического размера диска)
    FSUSE — сколько места на диске занято
    MOUNTPOINT — точка монтирования

MBR (Master Boot Record) - старый формат
GPT (GUID Partition Table) - новый UEFI

```sudo fdisk /dev/vdb``` - для управления диском, утилита

```sudo partprobe``` - перезагрузка файловой системы без рестарта ОС

```lvm``` — это система управления логическими дисками на одном или нескольких физических дисках без переразметки физических дисков
Подмонтирование физических дисков в корневую файловую систему.

LVM управляет тремя сущностями:
- Физический раздел (может включать в себя несколько физических дисков) — Physical Volume (PV)
- Группа физических дисков — Volume Group (VG)
- Логический диск — Logical Volume (LV)

Сначала под управление LVM нужно отдать физический диск или его раздел. Для этого инициализируйте его как PV (Physical Volume). Затем из PV создаётся VG (Volume Group). В VG создаются LV (Logical Volume), которые уже можно отформатировать с использованием определенной файловой системы и использовать для хранения данных
PV -> VG -> LV

![img.png](img.png)


```sudo pvs```
```sudo pvdisplay``` - подробная информация о тех pv которые отданы в lvm
```sudo pvcreate /dev/vdb1 /dev/vdb2``` - создание pv

```sudo vgs```
```sudo vgdisplay``` - подробная информация
```sudo vgcreate sausage-store /dev/vdb1 /dev/vdb2``` - создание группы vg 

```sudo lvs```
```sudo lvdisplay``` - подробно
```sudo lvcreate -l +100%FREE -n logs sausage-store``` - создаем логический том по группе

## Верхний уровень - файловая система

```sudo mkfs.ext4 /dev/my_vg/my_vol``` - создание файловой системы на диске
Диск будет доступен /dev/mapper/my_vg-my_vol

```sudo mount /dev/my_vg/my_vol /mnt/``` - монтирование в файловую систему 
Папку куда монтировать нужно создать, пример:
```
sudo mkdir -p /var/sausage-store/logs
sudo mount /dev/sausage-store/logs /var/sausage-store/logs)
```

Для монтирования после перезапуска команду надо прописать в файл /etc/fstab

Если добавляется еще диск в группу (для расширения),то потом надо расширить и файловую систему
```sudo resize2fs /dev/my_vg/my_vol``` - расширение файловой системы

```df -h``` - посмотреть файловую систему

```du -h -d 10 /var/log | sort -rh``` - просмотр больших файлов, сортировка

```ncdu /var/log``` - удобный интерфейс du (q - английский! выход)

# Права пользователя

```sudo chown пользователь:группа путь/к/файлу``` - изменение владельца файла/папки

# Softlinks hardlinks Ext4

```sudo ln -s file1 link1``` - создание символьной ссылки (содержит только лик на имя исходного файла)

Жесткая ссылка === имя файла, указатель на inode

```df -i``` - посмотреть статистику использования инод

```ln file2 link2``` - создать жесткую ссылку (добавляется указатель на inode)

```ls -la``` - вывод файлов в текущей директории
```ls -li``` - посмотреть файлы с инодами (первый столбец)
```find . -inum 115552019``` - смотрим кто ссылается на данную иноду

# Каналы (pipes) передача потока вывода из команды в другую команду (цепочка)

```ls | grep file | grep -v ssh``` - неименованная передача

```
mknod test_pipe p
cat file1 > test_pipe
```
Вывод в именованый канал не останавливается, его надо прерывать ctrl-c


# Практика

```lsof``` - просмотр действующих процессов

- COMMAND — процесс (команда), которая использует файл
- PID — идентификатор процесса
- USER — владелец процесса
- FD — дескриптор файла:


    cwd — рабочая директория
    rtd — директория root
    txt — исполняемый файл
    mem — область памяти
    число — это номер дескриптора файла, используемого процессом
    u — файл открыт с правами на чтение и запись
    r — файл открыт с правами на чтение
    w — файл открыт с правами на запись с частичной блокировкой файла
    W — файл открыт с правами на запись с блокировкой всего файла


- TYPE — тип файла:


    REG — файл
    DIR — директория
    FIFO — именованные каналы


- DEVICE — номер устройства, на котором находится файл
- SIZE/OFF — размер
- NODE — номер inode, индексного дескриптора
- NAME — имя файла

```lsof -i :{port}``` посмотреть кто использует порт

```sudo lsof -a +L1 /run``` - посмотреть файлы в папке (только удаленные), у которых не отпущены дескрипторы

## Виртуальная файловая система (/proc, /procfs)

Открытые файловые дескрипторы можно посмотреть в директории /proc/<PID процесса>/fd/

Файлы бывают следующих типов:

    ( - ) Обычный файл.
    ( d ) Директория — это объект файловой системы, который упрощает работу с файлами, позволяя группировать их.
    ( l ) Символическая cсылка.
    ( b ) Файл блочного устройства.
    ( c ) Файл символьного устройства.
    ( p ) Именованные каналы.
    ( s ) Сокет.
    ( n ) Сетевой файл.

## File links 

```ln -s /home/student/file1 link2``` - создание файловой ссылки (soft link)

inode - жесткая ссылка 

Файл - это ссылка на индексный дескриптор (inode)
```stat file1``` - показывает информацию inode у файла

убить процесс
kill -9 {pid}
использовать, для остановки http-server

посмотреть список открытых портов
```sudo netstat -ltupan```


# SUID, SGID, ACL расширенные права

```chmod u+s,a+rx``` - установка EUID, под каким пользователем будет запускаться
```chmod g+s,a+rx id``` - установка SGID

## Установка базовых прав на файл

```chmod ugo+rwx test0```
Где: u - пользователь, g - группа, o - прочие (others)
+/- - установка или сбро прав
rwx - на чтение, запись, запуск


https://help.ubuntu.ru/wiki/%D1%81%D1%82%D0%B0%D0%BD%D0%B4%D0%B0%D1%80%D1%82%D0%BD%D1%8B%D0%B5_%D0%BF%D1%80%D0%B0%D0%B2%D0%B0_unix
https://ruvds.com/ru/helpcenter/suid-sgid-sticky-bit-linux/
https://chmodcommand.com/chmod-775/

chmod g+775 htdocs

Расшифровка прав доступа

- чтение — обозначается как r (read),
- запись — обозначается как w (write),
- выполнение — обозначается как x (execute).
- suid/sgid обозначается на месте x - s (для юзера - запуск от владельца, или для группы - наследуются права)
- sticky - разрешение на удаление, только для владельца (в том числе и папки, в которой файл) - t на месте x (для прочих)

«drwxrwxrwx» (полный доступ всем) можно поделить на 4 секции: d rwx rwx rwx
1 секция можеть быть либо «d»- директория, либо «-» — файл.
2 секция показывает права доступа владельца файла или директории.
r-чтение, w-запись, x- выполнение.
3 секция показывает права доступа группы в которой находится владелец, rwx — аналогично
4 секция показывает права доступа все остальных пользователей системы.

https://habr.com/ru/articles/469667/

## Установка расширенных прав ACL

```setfacl -m default:u:front-user:rx /var/www-data/htdocs/``` - права по-умолчанию

```setfacl -d -m u:front-user:rx /var/www-data/htdocs/```

```getfacl file1``` - просмотр расширенных прав

# Файрвол iptables, firewalld

```sudo iptables -I INPUT 1 -s 0.0.0.0/0 -i eth0 -p tcp --dport 443 -j ACCEPT```
```sudo iptables -I INPUT 2 -s 0.0.0.0/0 -i eth0 -p tcp --dport 8080 -j DROP```


# Асимметричное шифрование, ключи

```ssh-keygen -t rsa -b 4096```  - генерация ключей
```ssh-keygen -t ecdsa -b 256``` - генерация новомодных ECDSA-ключей
Как отработает, получим публичный и приватный ключи, по умолчанию в домашнем каталоге 
в скрытой папке .ssh файлики id_rsa и id_rsa.pub

Для доступа к удалённой машине необходимо разместить публичный ключ в очередной 
строчке файла .ssh/authorized_keys

Для копирования публичного ключа на сервер можно использовать (для линукс):
```ssh-copy-id -i $HOME/.ssh/id_rsa.pub <имя пользователя>@<ip сервера куда хотим получить доступ>```

```ssh student@158.160.39.24``` - подключение из винды

Копирование ssh публичного ключа на нужный сервер
```type $env:USERPROFILE\.ssh\id_rsa.pub | ssh student@158.160.105.196 "cat >> .ssh/authorized_keys"```

Настройки подключения к различным серверам делать тут: .ssh/config
Пример:
```
Host gitlab-runner
  HostName 10.20.30.40
  User ya
  ForwardAgent yes
  IdentityFile ~/.ssh/id_rsa
  ProxyJump jump 
```

```StrictHostKeyChecking``` - принятие ключа от хостов с динамическим ip (вероятно, тогда нет необходимости в known_hosts)
В этом случае ключ будет добавлен в файл ~/.ssh/known_hosts автоматически
Важно!  Если ip адрес хоста динамически меняется, то каждый раз будет добавляться запись с новым ip.
Поэтому укажем /dev/null в качестве known_hosts файла
```UserKnownHostsFile=/dev/null```


Дефолтные настройки sshd (файл /etc/ssh/sshd_config), чтобы по ssh могли подключиться другие люди:
```
Port                            22
Protocol                        2
UsePAM                          yes
PermitRootLogin                 no
PasswordAuthentication          yes
PermitEmptyPasswords            no
PubkeyAuthentication            yes
AllowTcpForwarding              no
X11Forwarding                   no
StrictModes                     yes
UsePAM                          yes
PrintMotd                       no
PrintLastLog                    no
TCPKeepAlive                    yes
UseDNS                          no
LoginGraceTime                  1m
maxauthtries                    4
Subsystem                       sftp internal-sftp
AcceptEnv                       LANG LC_*
AllowAgentForwarding            yes
ChallengeResponseAuthentication no
AllowUsers                      ya root@IP
```
После изменения настроек ```sudo systemctl restart ssh```


# Работа с DNS

DNS управляется утилитой glibc, настройки тут: /etc/nsswitch.conf
hosts: files dns - сначала искать в файловом hosts, потом запрашивать DNS
/etc/hosts - локальные файловые хосты
/etc/resolv.conf - адреса DNS
Конфигурация DNS (systemd-resolved) находится тут: /etc/systemd/resolved.conf

# Монтирование папок

```sudo mount -t nfs 178.154.212.255:/opt/data/nfs /mnt/nfs``` - примонтировать NFS
```sudo umount /mnt/nfs``` - отмонтировать
```sudo showmount -e 178.154.212.255``` - посмотреть что доступно

Для монтирования при загрузке, добавить сюда: /etc/fstab строку:
178.154.212.255:/opt/data/nfs /mnt/nfs  nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0


## Практическая работа 2 задание 5

```sudo iptables -I INPUT 1 -s 0.0.0.0/0 -i eth0 -p tcp --dport 80 -j ACCEPT```
```sudo iptables -I INPUT 2 -s 0.0.0.0/0 -i eth0 -p tcp --dport 8080 -j DROP```

## Практическая работа 2 задание 8
```172.154.212.255:/var/www/images	            /mnt/images      nfs     defaults 0 0```

## Бонус

Установить exportfs!!!!!!!!
Прописать в /etc/exports
/mnt/pets 0.0.0.0(rw) 10.9.0.0/255.255.0.0(ro)


Доделать бонусное задание с vagrant


При восстановлении виртуалки для деплоя:
Установка node + nvm - устанавливать как root (sudo)
npm install http-server -g
https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-20-04
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
source ~/.bashrc
nvm list-remote
nvm install v22.20.2
npm install -g http-server

Установка java, 16
sudo apt update
java -version
если ее нет, будет выведен список откуда можно поставить - выбрать свежее 16
https://selectel.ru/blog/tutorials/how-to-install-java-on-ubuntu-20-04/

Установить mongo/postgres
Установить acl

Прокинуть переменные в конфигах!


