# Docker

```top -p 1764``` - приложение для отслеживания потребления ресурсов приложений по их PID

```docker ps``` - список запущенных контейнеров

```docker ps -a``` - список существующих контейнеров

```export PS1='> '``` - модификация строки шелл

```docker images``` - список образов

```docker exec -it hello bash``` - запустить баш в контейнере hello

- i — interactive режим
- t — чтобы был терминал
- hello — имя контейнера
- bash — команда, которую мы хотим выполнить

```docker history caddy-image``` - показать из каких слоев состоит этот образ

``` tee -a Caddyfile << END``` - создание файла, и ввод в него, END - команда завершения ввода

```
 docker rmi -f image my-caddy-image:0.0.1
 docker build --no-cache -t my-caddy-image:0.0.1 .
```
первая команда - для удаления образа
ключ --no-cache, чтобы образ пересобрался полностью. Т.к Докер кэширует слои при сборке
t - для указания имени нового образа

```docker start, docker stop, docker restart``` - управление контейнером

```docker logs -f <имя контейнера>``` - посмотреть логи внутри контейнера

```docker exec -it caddy curl localhost:8080``` - выполнить команду в контейнере

```docker run -d --restart=always -p 9999:8080 --name=caddy my-caddy-image:0.0.1```
always - количество рестартов
9999:8080 - проброс портов, 9999 - внешний, 8080 - внутренний в контейнере

```docker run --rm --name caddy caddy-image caddy run --config /caddy/Caddyfile```
запуск контейнера, если указано --rm то после его остановки он сразу удалиться

```docker rm -f caddy``` - f принудительное удаление (без остановки)

```docker cp <container_name>:<пусть к проекту> <путь куда копируем на текущем сервере>```
копирование файлов из контейнера

```bash
docker tag my-caddy-image:0.0.1 greenkeydoc/my-caddy-image:0.0.1
docker push <dockerhub username>/my-caddy-image:0.0.1 
```
Повесить тег, и выгрузить в докерхаб.
перед этим нужно залогиниться
```docker login -u greenkeydoc```


Создаем внутреннюю сеть sausage_network и запускаем контейнеры в нем!
```
docker network create -d bridge sausage_network 
# в папке backend:
docker run --rm -d --env-file .env --name sausage-backend --network=sausage_network sausage-backend:0.0.1
# в папке frontend:
docker run --rm -d --name sausage-frontend --network=sausage_network -p 8080:80 sausage-frontend:0.0.1
```




Лиз Райс «Безопасность контейнеров»
Статья на Хабре «Docker: что это и как используется в разработке».
Доклад «Docker в банке».
Книга «Использование Docker». Эдриен Моуэт.
Книга «Docker in Action. Second Edition». Jeff Nickoloff, Stephen Kuenzli.
Книга «Безопасность контейнеров». Лиз Райс.
Книга «Docker in Practice. Second Edition». Ian Miell and Aidan Hobson Sayers.


COPY . .  - не кэшируется
RUN - запускается при сборке
CMD - при запуске контейнера? выполняется только одна (последняя)
CMD ["npm", "start"] - вариант запуска
CMD - может быть переопределена!
ENTRYPOINT ["npm", "start"] 


# Полезные ссылки по докеру

Лучшие практики докера
https://docs.docker.com/build/building/best-practices/



Шпаргалка по командам
https://dockerlabs.collabnix.com/docker/cheatsheet/
![img.png](img.png)


# MinIO


```bash
rclone sync yandex-s3:std-034-60-rumynin minio:sample
```

```
crontab -e
0 * * * * rclone sync yandex-s3:std-034-60-rumynin minio:sample
```

# Vault

Инциализация после установки, и получение разделенного ключа:
```vault operator init```

```
Unseal Key 1: vexDWxzP+IDyDkWvpdRvcqemskI+4b7zsTMgpkfEGQIV
Unseal Key 2: 8Pefc2S6s3RKnVbMRMskqCjlRaNg5mi8n4Qqastmeb+8
Unseal Key 3: /FAWGHSlbVxKClMPQ1FZLnX5kPaTWpO6BgvIB8Z6To6q
Unseal Key 4: jc7AIlAgmqbI+KWdDIGa0sE1ljmmB6nMi1t1Z+TWUb09
Unseal Key 5: 5ZFR89QplqNHMBvawJdoW2Z4BgNlX7W9XsVT+TpemOE+

Initial Root Token: hvs.QHDbQzu2pPONWvSW8GO2FWt6
```

```valut operator unseal```
Для распечатывания нужно ввести 3 из 5 ключей, по команде

```
alias vault='sudo docker exec -it vault vault'
```
Для удобства работы с контейнером Vault - используем алиас

```vault login```
Для входа, чтобы работать с хранилищем


Управление ролями и токенами

```
# Тут нам понадобится heredoc, поэтому наш удобный алиас не подойдёт
$ sudo docker exec -i vault vault policy write sausage-store - <<EOF
# Policy name: sausage-store
#
# Read-only permission on 'secret/data/sausage-store' KV 2 path
path "secret/data/sausage-store" {
  capabilities = [ "read" ]
}
EOF

Success! Uploaded policy: sausage-store
```
создание политики


```
$ vault write auth/token/roles/sausage-store \
    allowed_policies="sausage-store" \
    orphan=true \
    period=8h

Success! Data written to: auth/token/roles/sausage-store
```
создание роли

```
$ vault token create -role=sausage-store

Key                  Value
---                  -----
token                hvs.CAESIBak8KWTeGgD8_P6CWVSDIniOO9fNJ1HrMW1znPwq0uYGh4KHGh2cy4yenVJaTVtcXZpekNCWUhpTGhFRjdMWUI
token_accessor       1rMoML5cc24iUxtaMMW9WUth
token_duration       8h
token_renewable      true
token_policies       ["default" "sausage-store"]
identity_policies    []
policies             ["default" "sausage-store"]
```
выпуск токена в рамках роли (с доступом по роли) - вручную.
в гитлабе заведен этап получения токена с запросом через jwt

Настройка аутентификации через jwt-токен gitlab

```
$ vault auth enable jwt
Success! Enabled jwt auth method at: jwt/ 
```
включение

```
$ vault write auth/jwt/config \
    jwks_url="https://gitlab.praktikum-services.ru/-/jwks" \
    bound_issuer="gitlab.praktikum-services.ru"
Success! Data written to: auth/jwt/config 
```
настраиваем связь с гитлабом

```
# Тут нам понадобится heredoc, поэтому наш удобный алиас не подойдет
sudo docker exec -i vault vault policy write sausage-store - <<EOF
# Policy name: sausage-store
#
# Read-only permission on 'secret/data/sausage-store' KV 2 path
path "secret/data/sausage-store" {
  capabilities = [ "read" ]
}
EOF
Success! Uploaded policy: sausage-store 
```
Добавим политику, разрешающую чтение секретов сосисочной

```
$ sudo docker exec -i vault vault write auth/jwt/role/sausage-store - <<EOF
{
  "role_type": "jwt",
  "policies": ["sausage-store"],
  "token_explicit_max_ttl": 1200,
  "user_claim": "user_login",
  "bound_claims": {
    "project_path": "std-010-056/sausage-store",
    "ref": ["main", "microservices", "balanced1", "balanced2", "deployment"],
    "ref_type": "branch"
  }
}
EOF
Success! Data written to: auth/jwt/role/sausage-store
```
связывания такой политики с JWT-аутентификацией нужно создать роль, в которой будут описаны требуемые поля токена
указываем что проверять по ветке, и указываем какие ветки!!!!! "main", "microservices", "balanced1"

